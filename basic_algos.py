def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)


# print(factorial(1))


def binary_search(data, target, low, high):
    if low > high:
        return False
    else:
        mid = (low + high) / 2
        if target == data[mid]:
            return True
        elif target < data[mid]:
            return binary_search(data, target, low, mid - 1)
        else:
            return binary_search(data, target, mid + 1, high)


# print(binary_search([2, 4, 5, 7, 8, 9, 12, 14, 17, 19, 22, 25, 27, 28, 33, 37], 22, 1, 9))


def nb_year(p0, percent, aug, p):
    pop_year = 0
    while int(p0) < p:
        p0 += p0 * percent / 100. + aug
        pop_year += 1
    return pop_year


# print(nb_year(1500, 5, 100, 5000))

# def find_it(seq):
#     return max(set(sorted(seq)), key=seq.count)


def find_it(seq):
    """https://www.codewars.com/kata/find-the-odd-int/"""
    for i in seq:
        if seq.count(i) % 2 != 0:
            return i


# print(find_it([20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5]))


def disemvowel(string):
    return "".join(s for s in string if s.lower() not in "aeiou")


# print(disemvowel("This website is for losers LOL!"))

# Greedy Algorithms - Partisan Problem K/2

def find_partition(int_list):
    "returns: An attempt at a partition of `int_list` into two sets of equal sum"
    a = set()
    b = set()
    for n in sorted(int_list, reverse=True):
        if sum(a) < sum(b):
           a.add(n)
        else:
           b.add(n)
    return (a, b)

# print(find_partition([2, 4, 6, 8, 9, 7, 32, 1 , 8]))


def get_sum(a, b):

    accumalator = 0

    if a == b:
        return a

    elif a > b:
        for i in range(b, a+1):
            accumalator += i
        return accumalator
    else:
        for i in range(a, b+1):
            accumalator += i
        return accumalator

    # a_list = []
    # if a == b:
    #     return a
    # else:
    #
    #     for i in range(a, b):
    #         a_list.append(i)
    #
    #     extra = sorted(a_list)[-1] + 1
    #     a_list.insert(int(len(a_list) + 1), extra)
    #     print(a_list)
    #     list_sum = int(sum(sorted(a_list)))
    #     return list_sum

# print(get_sum(0, -1))

def longest(s1, s2):
    # n = list(s1 + s2)
    # p = sorted(set(n))
    # c = "".join(p)
    # # return c
    return "".join(sorted(set(s1 + s2)))


# print(longest("aretheyhere", "yestheyarehere"))

# https://www.codewars.com/kata/regex-validate-pin-code/
import re
def validate_pin(pin):
    return bool(re.match(r'^(\d{4}|\d{6})$',pin))


def validate_pin(pin):
    import re
    if len(pin) == 4 or len(pin) == 6:  # not 4 or 6 digits
        if re.search('[^0-9]', pin) == None:  # contains non-digit chars
            return True

    return False



# from time import time
# start_time = time()
# hey = "@_@"
# end_time = time()
# elapsed = end_time - start_time
# # record the starting time
# # record the ending time
# # compute the elapsed time



def linear_sum(S, n):
    if n == 0:
        return 0

    else:
        return linear_sum(S, n - 1) + S[n - 1]

# print(linear_sum([4, 3, 6, 2, 8], 5))


def is_prime(n):
    if n == 1:
        return False

    for d in range(2, n):
        if n % d == 0:
            return False
    return True
#
# collect = []
#
# for n in range(1, 21):
#     print(n, is_prime(5))

    # print(prime_list)

# is_prime(5)


def twosum(nums, target):
    lookup = {}
    for cnt, num in enumerate(nums):
        if target - num in lookup:
            return lookup[target-num], cnt
        lookup[num] = cnt


print(twosum([2, 4, 6, 8, 10], 8))


# https://www.codewars.com/kata/complementary-dna/
def DNA_strand(dna):
    return dna.translate(str.translate("ATCG","TAGC"))


def fibbo(n):
    if n == 0:
        return 1
    else:
        return fibbo(n - 1) + fibbo(n + 1)

#
# print(fibbo(5))


def find(n):
    # l = []
    # for num in range(n + 1):
    #     if num % 3 == 0 or num % 5 == 0:
    #         if num != 0:
    #             l.append(num)
    # print(l)
    # return sum(l)
    return sum(num for num in range(1, n+1) if num % 3 == 0 or num % 5 == 0)
# print(find(5))


def primality(n):
    if n < 2:
        return False
    else:
        for i in range(2, n):
            if n % 2 == 0:
                return False
        return True


def is_prime_new(n):
    if n == 1:
        return False

    for d in range(2, n):
        if n % d == 0:
            return False
    return True
print(is_prime_new(15))


def merge_sort(arr):
    # [4, 8, 7, 5, 3, 2, 2]
    if len(arr) < 2:
        return arr

    result = []

    mid = int(len(arr) / 2)
    first_half = merge_sort(arr[:mid])
    second_half = merge_sort(arr[mid:])

    while (len(first_half) > 0) and (len(second_half) > 0):
        if first_half[0] > second_half[0]:
            result.append(first_half[0])
            first_half.pop(0)
        else:
            result.append(second_half[0])
            second_half.pop(0)
        result += first_half
        result += second_half

        return result


# print(merge_sort([4, 5, 3, 6, 9, 2, 1]))

def msort3(x):
    result = []
    if len(x) < 2:
        return x
    mid = int(len(x) / 2)
    y = msort3(x[:mid])
    z = msort3(x[mid:])
    i = 0
    j = 0
    while i < len(y) and j < len(z):
        if y[i] > z[j]:
            result.append(z[j])
            j += 1
        else:
            result.append(y[i])
            i += 1
    result += y[i:]
    result += z[j:]
    return result


# print(msort3([4, 5, 3, 6, 9, 2, 1]))


def msort4(x):
    result = []
    if len(x) < 20:
        return sorted(x)
    mid = int(len(x) / 2)
    y = msort4(x[:mid])
    z = msort4(x[mid:])
    i = 0
    j = 0
    while i < len(y) and j < len(z):
        if y[i] > z[j]:
            result.append(z[j])
            j += 1
        else:
            result.append(y[i])
            i += 1
    result += y[i:]
    result += z[j:]
    return result


# print(msort4([4, 5, 3, 6, 9, 2, 1]))

def test2():
    l = [2, 4, 5, 6]
    l.pop(3)

    return l
print(test2())


def dfs_recursive(graph, vertex, path=[]):
    path += [vertex]

    for neighbor in graph[vertex]:
        if neighbor not in path:
            path = dfs_recursive(graph, neighbor, path)

    return path


adjacency_matrix = {1: [2, 3], 2: [4, 5],
                    3: [5], 4: [6], 5: [6],
                    6: [7], 7: []}

print(dfs_recursive(adjacency_matrix, 1))

# USE THIS
def dfs_iterative(graph, start):
    stack, path = [start], []

    while stack:
        vertex = stack.pop(0)
        if vertex in path:
            continue
        path.append(vertex)
        for neighbor in graph[vertex]:
            stack.append(neighbor)

    return path


adjacency_matrix = {1: [2, 3], 2: [4, 5],
                    3: [5], 4: [6], 5: [6],
                    6: [7], 7: []}

print(dfs_iterative(adjacency_matrix, 1))
