"""
Traverse an N*M matrix clockwise. Start from the out most layers, and move to the inner most layer in a spiral fashion. For example, if the matrix is
1 3 4 2 1
2 2 3 1 1
4 1 2 1 4
the out put is 1 3 4 2 1 1 4 1 2 1 4 2 2 3 1
"""
matrix = [1, 3, 4, 2, 1,
          2, 2, 3, 1, 1,
          4, 1, 2, 1, 4]
matrix_length = len(matrix)

matrix_n = matrix[:5]
matrix_m = matrix[9:10]

"""
Interview Questions

I introduced myself.
He jumped to coding question :
First question : Given a sorted array which has been rotated k times , need to find k?
Second Question : Given a binary tree find the maximum sum from leaf to leaf?
THird Question : Same as Second now i need to optimize it to work from any node to node?  
"""

# Problem Name is &&& Pangram &&& PLEASE DO NOT REMOVE THIS LINE.

"""

 Pangram Detector

 The sentence "The quick brown fox jumps over the lazy dog" contains
 every single letter in the alphabet. Such sentences are called pangrams.

 Write a function findMissingLetters, which takes a string `sentence`,
 and returns all the letters it is missing (which prevent it from
 being a pangram). You should ignore the case of the letters in sentence,
 and your return should be all lower case letters, in alphabetical order.
 You should also ignore all non US-ASCII characters.


from string import maketrans   # Required to call maketrans function.

intab = "aeiou"
outtab = "12345"
trantab = maketrans(intab, outtab)

str = "this is string example....wow!!!";
print str.translate(trantab, 'xm')

"""

ALPHABET = 'abcdefghijklmnopqrstuvwxyz'


# TODO: complete this function stub
def FindMissingLetters(sentence):
    result = []
    l = []
    #     [s, e, n, t, e, n , c, e]
    # for index, charac in enumerate(sentence):
    #     for c in ALPHABET:
    #         if charac == c:
    #             l.remove(charac)
    chars = list(ALPHABET)
    for s in sentence.lower():
        if s in chars:
            chars.remove(s)
    new_chars = "".join(chars)
    # print(new_chars)

    return new_chars


# Test Cases
def runTests():
    success = (
            '' == FindMissingLetters("The quick brown fox jumps over the lazy dog") and
            'bfgjkvz' == FindMissingLetters("The slow purple oryx meanders past the quiescent canine") and
            'cdfjklmopqruvxyz' == FindMissingLetters("We hates Bagginses!") and
            'abcdefghijklmnopqrstuvwxyz' == FindMissingLetters("")
    )

    return success


if __name__ == "__main__":
    if runTests():
        print("All tests passed")
    else:
        print("At least one test failed.")
