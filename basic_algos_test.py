import unittest
from basic_algos import factorial, find_it


class test_basic_algos(unittest.TestCase):
    def test_factorial(self):
        self.assertEquals(factorial(3), 6)

    def test_find_it(self):
        self.assertEquals(find_it([20, 1, -1, 2, -2, 3, 3, 5, 5, 1, 2, 4, 20, 4, -1, -2, 5]), 5)




if __name__ == '__main__':
    unittest.main()